﻿SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `itentiser`;

CREATE TABLE IF NOT EXISTS `itentiser` (
`id_itentiser` int(11) NOT NULL AUTO_INCREMENT
`prenom` varchar(30) not null,
`nom` varchar(30) not null
`mdp` varchar(256) not null,
PRIMARY KEY (`id_itentiser`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `comment` (
  `id_comment` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  `prenom` text NOT NULL,
  `id_itentiser` int(11) unique not null,
  `message` text,
  PRIMARY KEY (`id_comment`),
	FOREIGN KEY (`id_itentiser`) REFERENCES itentiser(`id_itentiser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
