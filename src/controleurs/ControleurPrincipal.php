<?php

namespace intent\controleurs;

use \intent\vues\VuePrincipale;
use \intent\models\Personality;


/**
*  Classe qui répertorie les fonctions principales et globales du site
*/
class ControleurPrincipal{

  /**
  * Affiche la page d'accueil du site
  */
  public function afficherAccueil(){
      $v=new VuePrincipale(VuePrincipale::ACCUEIL);
      $v->render();
  }

  /**
  * Affiche la page de profil utilisateur (grace a la barre de recherche sur la page d'accueil)
  */
  public function afficherPageProfil($id_personality=null){
    try{
      if($id_user!=null){
        //On vérifie id_liste
        if(!filter_var($id_user, FILTER_VALIDATE_INT))
          throw new \Exception("id_user incorect");

        $user = Utilisateur::where('id_user', '=', $id_user)->first();
      }
      else{
        if(!filter_var($_POST['recherche_email_personne'], FILTER_VALIDATE_EMAIL))
          throw new \Exception("email rentré invalide");

        $user = Utilisateur::where('email', '=', $_POST['recherche_email_personne'])->first();
      }

      if(!isset($user) || $user==null)
        throw new \Exception("utilisateur inconnu");

      $listes=Liste::where('id_user', '=', $user->id_user)->where('token', '!=', null)->get();

      $v=new VuePrincipale(VuePrincipale::UTILISATEUR, $user, $listes);
      $v->render();
    }
    catch (\Exception $e){
      $v = new VuePrincipale(VuePrincipale::ERREUR, "Impossible d'acceder à l'utilisateur: ".$e->getMessage());
      $v->render();
    }
  }

  public function afficherPageAideInfo(){

  }
}
