<?php

namespace intent\models;

/*
* création de l'objet utilisateur et de la correspondance avec la base de donnéeS
*/
class Personality extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'personality';
    protected $primaryKey = 'id_personality';
    public $timestamps = false;
}
