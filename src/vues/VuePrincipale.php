<?php

namespace intent\vues;

use \Slim\Slim;
use intent\models\Personality;

/**
* Classe qui permet d'afficher les principaux élément du site
*/
class VuePrincipale{

  private $type, $parametres;
  const ACCUEIL=1;
  const PROFIL=2;

  /**
  * Construit une VuePrincipale
  *   @param $t, int option pour la vue
  *   @param $par, mixed deuxieme parametre généralement egale a la liste
  */
  public function __construct($t, $par=null){
    $this->type=$t;
    $this->parametres=$par;
  }

  /**
  * Affiche la page html que le navigateur va interpreter
  */
  public function render(){
    $head=$this->afficherHead();
    $header=$this->afficherHeader();
    $pied=$this->afficherPiedPage();
    $root=Slim::getInstance()->request->getRootUri();

    switch($this->type){
      case VuePrincipale::ACCUEIL:
        $principal=$this->afficherAccueil();
        break;
      case VuePrincipale::PROFIL:
        $principal=$this->afficherUtilisateur();
        break;
      case VuePrincipale::ERREUR:
        $titre="ERREUR";
        $message=$this->param;
        $lien="$root/$this->param2";
        $principal=$this->pageInformation($titre, $message, $lien);
        break;
      case VuePrincipale::INFO:
        $titre="INFORMATION";
        $message=$this->param;
        $lien="$root/$this->param2";
        $principal=$this->pageInformation($titre, $message, $lien);
        break;
    }

    echo <<<END
  <!DOCTYPE html>
    <html lang="fr">
      $head
      <body>
        $header
        $principal
        $pied
      </body>
    </html>
END;
  }

  /**
  * Génère le head, debut de la page html
  *   @return String correspondant à la balise head
  */
  public function afficherHead(){
    $root=Slim::getInstance()->request->getRootUri();
    return <<<END
    <head>
      <title>intent</title>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link rel="stylesheet" href="$root/style.css" />
      <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
END;
  }

  /**
  * Génère le header, entete de la page
  *   @return String correspondant au à la balise header
  */
  public function afficherHeader(){
    $root=Slim::getInstance()->request->getRootUri();

    return <<<END
<header>
  <h1>intent</h1>
</header>
END;
  }

  /**
  * Génère le contenu de l'accueil
  *   @return String correspondant a code html pour l'accueil
  */
  public function afficherAccueil(){

    return <<<END
<form class="search" method="post" action="">
  <input type="text" name="search_personality" class="searchTerm" placeholder="Rechercher quelqu'un"><button type="submit" class="searchButton"><i class="fa fa-search"></i></button>
</form>
<div class="div_creation">
  <h2 style="text-align: center;">Bienvenue sur intent !</h2>
  <p>Fini les arnques, avec intent vous pouvez déterminer si une personnalité est fiable ou non grâce au notes et aux commentaires de autres internautes.</p>
</div>
END;
  }

  /**
  * Génère le contenu du pied de page
  *   @return String correspondant a code html pour le footer
  */
  public function afficherPiedPage(){
    $root=Slim::getInstance()->request->getRootUri();
    return
<<<END
    <footer>
      <p style='float:left; display:inline-block;'>&nbsp;&nbsp Developped by mindware for leroisdesrats</p>
      <a style='float:right;  color:white;' href="$root/aide"><p>Besoin d'aide ? &nbsp</p></a>
    </footer>
END;
  }

  /**
  * Génère le contenu d'une page d'information
  *   @param $titre String titre de la page d'info
  *   @param $message String message d'information
  *   @param $lienRetour String lien pour le bouton retour
  *   @return String correspondant a code html pour la page d'information
  */
  public function pageInformation($titre, $message, $lienRetour){
    return <<<END
      <div class="div_creation">
        <h4 style="text-align: center;">$titre</h4>
        <p>$message</p>
        <a class='button' style='float:none; margin-left: 45%;' href='$lienRetour'><p>OK</p></a>
      </div>
END;
  }

  /**
  * Génère le contenu pour afficher un profil utilisateur
  *   @return String code html pour le contenu de la page d'un utilisateur
  */
  public function afficherProfil(){
    $root=Slim::getInstance()->request->getRootUri();

      return <<<END
<div id='div_entete'>
  <div class='div_contenu'><h1>$user->prenom $user->nom</h1><h2><i>Voici ses listes</i></h2></div><div class='div_action'><p>$user->email</p></div>
  </div><div id='div_liste'>
    $lignes
  </div>
END;
    }
}
