<?php

require_once 'vendor/autoload.php';

use \intent\controleurs\ControleurPrincipal;
use \Illuminate\Database\Capsule\Manager;
use \Slim\Slim;

$app = new Slim();
$config = parse_ini_file('conf/conf.ini');
$DB = new Manager();
$DB->addConnection($config);
$DB->setAsGlobal();
$DB->bootEloquent();

$app->get("/accueil", function () {
  $c=new ControleurPrincipal();
  $c->afficherAccueil();
});

$app->get("/", function () {
  $c=new ControleurPrincipal();
  $c->afficherAccueil();
})->name('accueil');

$app->post("/profil", function () {
  $c=new ControleurPrincipal();
  $c->afficherPageProfil();
});

$app->get("/profil/:id_personality", function ($id_personality) {
  $c=new ControleurPrincipal();
  $c->afficherPageProfil($id_personality);
});

$app->get("/aide", function () {
  $c = new ControleurPrincipal();
  $c->afficherPageAideInfo();
});

$app->run();
